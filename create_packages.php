#!/usr/bin/env php
<?php

/**
 * @file
 * This script regenerates the packages.json file.
 */

// The URL of the Gitlab server.
define('UW_WCMS_TOOLS_GITLAB_SERVER', 'https://git.uwaterloo.ca/');
define('UW_WCMS_TOOLS_GITLAB_API_PATH', 'api/v4/');

define('WCMS_CURRENT_BRANCH', '3.2.x');

// Gitlab private token. For CLI use, get it from the environment. Otherwise,
// use the Drupal variable. Almost all the functions in this file depend on
// having this set.
if (php_sapi_name() === 'cli') {
  $token = getenv('GITLAB_PRIVATE_TOKEN');
  if (!$token) {
    throw new Exception('GITLAB_PRIVATE_TOKEN environment variable not set.');
  }
}
else {
  $token = variable_get('gitlab_private_token');
  if (!$token) {
    throw new Exception('Drupal variable "gitlab_private_token" not set.');
  }
}
define('UW_WCMS_TOOLS_GITLAB_PRIVATE_TOKEN', $token);
unset($token);


echo "Starting process (this will take a few minutes)...\n";

$all_projects = uw_wcms_packages_get_all_projects();

// Create variable with project versions used in drupal-recommended-project.
$drp_composer = uw_wcms_packages_gitlab_call('wcms/drupal-recommended-project/raw/' . WCMS_CURRENT_BRANCH . '/composer.json');
$composer_require_versions = ($drp_composer['require'] ?? []) + ($drp_composer['require-dev'] ?? []);
// Remove "dev-" from the start of the version if it is there.
foreach ($composer_require_versions as $key => $value) {
  if (substr($value, 0, 4) === 'dev-') {
    $composer_require_versions[$key] = substr($value, 4);
  }
}

// Start the JSON output.
$json = [];

// Step through all the projects and get the correct JSON output.
foreach ($all_projects as $project) {
  echo 'Processing ' . $project['namespace'] . '/' . $project['name'] . "...\n";

  // If there are branches from the project, process them.
  uw_wcms_packages_process_tags_branches($project, 'branches', $json, $composer_require_versions);

  // If there are tags in the project, process them.
  uw_wcms_packages_process_tags_branches($project, 'tags', $json, $composer_require_versions);

  echo "\n";
}

// Open the packages,json file.
$json_file = fopen("packages.json", "w") or die("Unable to open file!");

// Write the json to the packages.json file.
fwrite($json_file, json_encode($json, JSON_PRETTY_PRINT));

// Close the packages.json file.
fclose($json_file);

// Show that the program has completed.
echo 'completed on ' . date('l, F j\, Y \a\t h:i:sa.') . "\n";

/**
 * Returns an array of projects, branches, and tags from GitLab.
 *
 * @return array
 *   The project data.
 */
function uw_wcms_packages_get_all_projects() {
  // CURL setup to access the groups (namespace) of the UW Gitlab.
  echo "Getting gitLab API project list\n";
  $groups[] = uw_wcms_packages_gitlab_call(UW_WCMS_TOOLS_GITLAB_API_PATH . 'groups/wcms');
  $groups[] = uw_wcms_packages_gitlab_call(UW_WCMS_TOOLS_GITLAB_API_PATH . 'groups/drupal-org');
  $groups[] = uw_wcms_packages_gitlab_call(UW_WCMS_TOOLS_GITLAB_API_PATH . 'groups/libraries');

  echo "Done getting gitLab API project list\n";

  // Step through each of the groups and process the tags and branches from UW
  // Gitlab.
  $all_projects = [];
  foreach ($groups as $group) {
    // FOR TESTING: Just one group below.
    // Limit to only one group so all groups do not get process.
    // @code
    // if (!in_array($group['path'], ['wcms'], TRUE)) {
    //  continue;
    // }
    // @endcode
    if (!in_array($group['path'], ['wcms', 'drupal-org', 'libraries'], TRUE)) {
      continue;
    }
    $gid = $group['id'];

    // Get all the projects decoded from the CURL request of projects based on a
    // group.
    $projects = uw_wcms_packages_gitlab_call_all_pages(UW_WCMS_TOOLS_GITLAB_API_PATH . 'groups/' . $gid . '/projects');

    // Step through each project and setup the tags and branches.
    foreach ($projects as $project) {
      // FOR TESTING: Just one project below.
      // @code
      // if ($project['path_with_namespace'] !== 'drupal-org/office_hours') {
      //   continue;
      // }
      // @endcode
      if ($project['path_with_namespace'] == 'wcms/drupal-recommended-project') {
        // We don't need DRP in the packages and it takes up a lot of lines.
        continue;
      }

      // Output name of each project acted on.
      echo $project['path_with_namespace'] . ': ' . $project['id'] . "\n";

      uw_wcms_packages_get_tags_branches($project, 'tags', $all_projects);
      uw_wcms_packages_get_tags_branches($project, 'branches', $all_projects);

      // Check each of the project names and ensure that the namespace is set to
      // drupal or the given (i.e. wcms).
      if (isset($all_projects[$project['path_with_namespace']])) {
        if (count($all_projects[$project['path_with_namespace']]) > 0) {
          if ($project['namespace']['path'] == 'drupal-org') {
            $all_projects[$project['path_with_namespace']]['namespace'] = 'drupal';
          }
          else {
            $all_projects[$project['path_with_namespace']]['namespace'] = $project['namespace']['path'];
          }

          $all_projects[$project['path_with_namespace']]['name'] = $project['name'];
          $all_projects[$project['path_with_namespace']]['path_with_namespace'] = $project['path_with_namespace'];
        }
      }
    }
  }
  return $all_projects;
}

/**
 * Get tag and branch information for repositories in a project.
 *
 * @param array $project
 *   The array of projects.
 * @param string $key
 *   The array key in $projects to look at. Either "tags" or "branches".
 * @param array $all_projects
 *   The array to fill with project data.
 */
function uw_wcms_packages_get_tags_branches(array $project, $key, array &$all_projects) {
  // Setup the CURL request to get the tags based on the project.
  $tags = uw_wcms_packages_gitlab_call_all_pages(UW_WCMS_TOOLS_GITLAB_API_PATH . 'projects/' . $project['id'] . '/repository/' . $key);

  // Step through each tag and only get out the ones with 8.x- or 9.x-
  // or that are Libraries.
  foreach ($tags as $tag) {
    if (!isset($tag['name'])) {
      echo 'Error: Invalid ' . $key . ".\n";
      continue;
    }
    $namespaces = ['drupal-org', 'wcms'];
    if (substr($tag['name'], 0, 4) === '8.x-' || substr($tag['name'], 0, 4) === '9.x-' || (in_array($project['namespace']['path'], $namespaces, TRUE) && preg_match('/(^feature\/(?:ISTWCMS|RT)-?\d+-.+|^\d+\.\d+\.(\d+|x$)|\d+\.\d+\.x-uw_wcms\d+)/', $tag['name']))) {
      $all_projects[$project['path_with_namespace']][$key][] = $tag;
    }
    elseif ($project['namespace']['name'] == 'Libraries') {
      if (!preg_match('/[0-7].x/', $tag['name'])) {
        if ($tag['name'] === 'master' || preg_match('/^v?\d+\.\d+\.\d+(-(alpha|beta|rc|patch)\.?\d*)?$/', $tag['name'])) {
          $all_projects[$project['path_with_namespace']][$key][] = $tag;
        }
      }
    }
  }
}

/**
 * Process tag and branch data, looking up additional information from the API.
 *
 * @param array $project
 *   The array of projects.
 * @param string $key
 *   The array key in $projects to look at. Either "tags" or "branches".
 * @param array $json
 *   The output array.
 * @param array $composer_require_versions
 *   An array of package tags to always include in the output.
 */
function uw_wcms_packages_process_tags_branches(array $project, $key, array &$json, array $composer_require_versions) {
  if (!isset($project[$key])) {
    return;
  }

  $project_namespace_name = $project['path_with_namespace'];
  $project_namespace_name = str_replace('drupal-org/', 'drupal/', $project_namespace_name);

  $counter = 0;
  $major_version_counter = 0;
  $last_major_version = '';

  // Sort the array so higher version numbers are guaranteed to be listed first.
  usort($project[$key], function ($a, $b) {
    // Treat "uw_wcms" tags as an additional version revision.
    $a['name'] = str_replace('-uw_wcms', '.', $a['name']);
    $b['name'] = str_replace('-uw_wcms', '.', $b['name']);
    // Handle cases where only one string has a Drupal core version reference.
    $a_has_core = preg_match('/^\d\.x-/', $a['name']);
    $b_has_core = preg_match('/^\d\.x-/', $b['name']);
    if ($a_has_core && !$b_has_core) {
      return 1;
    }
    elseif (!$a_has_core && $b_has_core) {
      return -1;
    }
    else {
      // Either both or no strings have Drupal core version references.
      return version_compare($a['name'], $b['name'], '<') ? 1 : -1;
    }
  });

  // Step through each tag and process it.
  foreach ($project[$key] as $tag) {
    $matches = NULL;
    $dev_snapshot_tag = FALSE;
    $uw_wcms_tag = FALSE;
    echo ucfirst($key) . ': ' . $project['name'] . ' ' . $tag['name'] . ': ';
    // Skip invalid tags and branches.
    if ($project['namespace'] === 'drupal' || $project['namespace'] === 'wcms') {
      if (preg_match('/unstable/', $tag['name'])) {
        echo "Skipping Unstable\n";
        continue;
      }
      if ($key === 'tags') {
        // Possible tags:
        // @code
        // 3.0.x-uw_wcms1
        // 1.2.3
        // 8.x-1.0-alpha5
        // 8.x-1.0-alpha5+5-dev
        // 8.x-1.0-alpha5-uw_wcms1
        // !8.x-1.0-alpha5-uw_wcms
        // !8.x-1.0-alpha-uw_wcms2
        // 8.x-1.0-uw_wcms4
        // !8.x-1.0-uw_wcms
        // !8.x-1.0-ABC-uw_wcms5
        // @endcode
        $valid_regular_tag = (bool) preg_match('/^(?:(\d+\.x)-)?(\d+(?:\.\d+\.x|(?:\.\d+){1,2})(?:-(?:alpha|beta|rc|patch)\.?\d+)?(\+\d+-dev)?(-uw_wcms\d+)?)$/', $tag['name'], $matches);
        if (!$valid_regular_tag) {
          // Not a valid tag.
          echo "Skipping Tag\n";
          continue;
        }
        if (!empty($matches[3])) {
          $dev_snapshot_tag = TRUE;
        }
        if (!empty($matches[4])) {
          // Match 3 = -uw_wcms#. If they exist then they are uw_wcms tag.
          $uw_wcms_tag = TRUE;
        }
      }
      elseif ($key === 'branches') {
        // Possible branches:
        // @code
        // feature/ISTWCMS-# || feature/ISTWCMS#
        // - made for feature/ISTWCMS-[jira-ticket-#]-[userid]-[description]
        // - made for feature/ISTWCMS[jira-ticket-#]-[userid]-[description]
        // feature/RT-# || feature/RT#
        // - made for feature/RT-[#]-[userid]-[description]
        // - made for feature/RT[#]-[userid]-[description]
        // 8.x-1.x
        // !8.x-1.0
        // 8.x-1.x-uw_wcms
        // 8.x-1.0-uw_wcms
        // !8.x-1.0-alpha5
        // !8.x-1.x-alpha
        // !8.x-1.x-alpha5
        // 8.x-1.0-alpha5-uw_wcms
        // !8.x-1.0-alpha5-uw_wcms1
        // !8.x-1.x-alpha5-uw_wcms
        // !8.x-1.x-ABC-uw_wcms
        // !8.x-1.0-ABC-uw_wcms
        // 8.x-1.x-ISTWCMS1234-description-goes-here
        // 8.x-1.x-ISTWCMS-1234-description-goes-here
        // 8.x-1.x-RT1234-description-goes-here
        // 8.x-1.x-RT-1234-description-goes-here
        // 8.x-1.x-userid-description-goes-here
        // @endcode
        if (!preg_match('/^feature\/(?:ISTWCMS|RT)-?\d+-.+$|(?:(\d+\.x)-)?(\d+\.x(-uw_wcms|-(?:(?:ISTWCMS|RT)-?\d+|[a-z][a-z0-9]{1,7})-.+)?|\d+\.\d+(?:-(?:alpha|beta|rc)\d+)?(-uw_wcms))$/', $tag['name'], $matches)) {
          // Not a valid branch.
          echo "Skipping Branch\n";
          continue;
        }
      }
    }
    // Library with WCMS tag. Need to prefix with 'dev-' and use full ref name.
    elseif (strpos($tag['name'], 'uw_wcms') !== FALSE) {
      $uw_wcms_tag = TRUE;
      $dev_snapshot_tag = TRUE;
    }

    if (empty($matches)) {
      preg_match('/^(\d+\.x)-(\d+\..+)$/', $tag['name'], $matches);
    }

    $core = isset($matches[1]) ? $matches[1] : NULL;

    // Special case for feature branches.
    if (isset($matches[0]) && substr($matches[0], 0, 8) == 'feature/') {
      $tag_name = $tag['name'];
    }
    else {
      $tag_name = ($core && isset($matches[2])) ? $matches[2] : $tag['name'];
    }

    // When dealing with tags, keep track of major versions.
    if ($key === 'tags') {
      preg_match('(^\d+\.\d+)', $tag_name, $matches);
      $current_major_version = $matches[0] ?? $tag_name;
      if ($current_major_version !== $last_major_version) {
        $last_major_version = $current_major_version;
        $major_version_counter++;
        $counter = 0;
      }
    }

    // Skip some tags. Do all branches, all tags that are referred to in DRP,
    // and some recent tags (X each of the X most recent versions).
    $max_tags = 3;
    $max_versions = 4;
    // We subtract one from $max_tags because this counter starts at 0.
    if ($key === 'tags' && isset($composer_require_versions[$project_namespace_name]) && ($composer_require_versions[$project_namespace_name] === $tag_name || $composer_require_versions[$project_namespace_name] === '8.x-' . $tag_name)) {
      if ($composer_require_versions[$project_namespace_name] === '8.x-' . $tag_name) {
        $tag_name = '8.x-' . $tag_name;
      }
      echo "Adding tag $tag_name mentioned in composer.json\n";
    }
    elseif ($key === 'tags' && $uw_wcms_tag) {
      if ($composer_require_versions[$project_namespace_name] === '8.x-' . $tag_name) {
        $tag_name = '8.x-' . $tag_name;
      }
      echo "Adding tag $tag_name because it's ours.\n";
    }
    elseif ($key === 'tags' && ($counter++ > $max_tags - 1 || $major_version_counter > $max_versions)) {
      echo "Skipping $project_namespace_name ($tag_name); maximum number generated\n";
      continue;
    }

    $composer_json = uw_wcms_packages_gitlab_call($project['path_with_namespace'] . '/raw/' . $tag['name'] . '/composer.json', FALSE);

    // Take type information from composer.json.
    if ($composer_json) {
      // Copy entire composer.json file for type composer-plugin.
      if (isset($composer_json['type']) && $composer_json['type'] === 'composer-plugin') {
        $json['packages'][$project_namespace_name][$tag_name] = $composer_json;
      }
      // Otherwise, only copy select keys.
      else {
        foreach (['type', 'extra', 'require'] as $json_key) {
          if (isset($composer_json[$json_key])) {
            $json['packages'][$project_namespace_name][$tag_name][$json_key] = $composer_json[$json_key];
          }
        }
      }
    }
    else {
      echo 'Composer file not found. ';
    }

    // Skip version if any require versions are invalid.
    foreach ($json['packages'][$project_namespace_name][$tag_name]['require'] ?? [] as $version) {
      if ($version === 'dev') {
        echo "Invalid require version\n";
        unset($json['packages'][$project_namespace_name][$tag_name]);
        continue;
      }
    }

    // Ensure there is always a type.
    if (empty($json['packages'][$project_namespace_name][$tag_name]['type'])) {
      echo 'No type found; using ';
      if ($project['namespace'] == 'libraries') {
        $json['packages'][$project_namespace_name][$tag_name]['type'] = 'wcms-lib';
      }
      else {
        $json['packages'][$project_namespace_name][$tag_name]['type'] = $project['namespace'] . '-module';
      }
    }
    // Always echo type used.
    echo $json['packages'][$project_namespace_name][$tag_name]['type'] . "\n";

    // Store the name of the tag, which is the project namespace/name.
    $json['packages'][$project_namespace_name][$tag_name]['name'] = $project_namespace_name;

    // Store the reference to the tag, which is the tag name.
    $json['packages'][$project_namespace_name][$tag_name]['source']['reference'] = $tag['name'];

    // Store the type as git.
    $json['packages'][$project_namespace_name][$tag_name]['source']['type'] = "git";

    // Store the URL.
    $json['packages'][$project_namespace_name][$tag_name]['source']['url'] = UW_WCMS_TOOLS_GITLAB_SERVER . $project['path_with_namespace'] . '.git';

    // Custom versioning to by-pass composer from throwing errors and not
    // finding/installing modules/themes.
    if ($uw_wcms_tag && !$dev_snapshot_tag) {
      // Need to add dev- or composer will reject it.
      $json['packages'][$project_namespace_name][$tag_name]['version'] = 'dev-' . $tag_name;
    }
    elseif ($key === 'tags' && !$dev_snapshot_tag) {
      $json['packages'][$project_namespace_name][$tag_name]['version'] = $tag_name;
    }
    else {
      // Branch or snapshot tag. Have to add dev- in front.
      $json['packages'][$project_namespace_name][$tag_name]['version'] = 'dev-' . $tag['name'];
    }

    // Drupal-specific properties.
    if (in_array($project['namespace'], ['drupal', 'wcms'], TRUE)) {
      $json['packages'][$project_namespace_name][$tag_name]['extra']['drupal']['version'] = $tag['name'];
      $json['packages'][$project_namespace_name][$tag_name]['extra']['drupal']['datestamp'] = strtotime($tag['commit']['authored_date']);
      $json['packages'][$project_namespace_name][$tag_name]['extra']['drupal']['package'] = $project['name'];
      $json['packages'][$project_namespace_name][$tag_name]['extra']['drupal']['core'] = $core;
    }
  }
}

/**
 * Make an HTTP call to Gitlab.
 *
 * @param string $path
 *   The path to make the call to.
 * @param bool $throw_on_failure
 *   If curl_exec() returns FALSE, if this is TRUE, throw, otherwise, return
 *   NULL.
 *
 * @return string|null
 *   The decoded JSON response or NULL.
 */
function uw_wcms_packages_gitlab_call($path, $throw_on_failure = TRUE) {
  $ch = curl_init(UW_WCMS_TOOLS_GITLAB_SERVER . $path);
  $header = ['PRIVATE-TOKEN: ' . UW_WCMS_TOOLS_GITLAB_PRIVATE_TOKEN];
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_ENCODING, 'gzip');

  // The results from the CURL request and the close.
  $result = curl_exec($ch);

  if ($result === FALSE) {
    if ($throw_on_failure) {
      throw new Exception('HTTP failure.');
    }
  }
  elseif ($result === "Retry later\n") {
    throw new Exception('GitLab returned "Retry later". Rate limit may have been exceeded.');
  }
  else {
    return json_decode($result, TRUE);
  }
}

/**
 * Access the GitLab API with pagination.
 *
 * @param string $path
 *   The API path to make the call to.
 * @param array $params
 *   The parameters to add to the query.
 *
 * @return array
 *   All pages of the query results.
 */
function uw_wcms_packages_gitlab_call_all_pages(string $path, array $params = []) {
  // The number per page to retreive. The most GitLab allows is 100.
  $params['per_page'] = 100;
  // The current page we are on.
  $params['page'] = 0;

  $results = [];

  while (TRUE) {
    // Increment the page that we are looking at.
    $params['page']++;
    // Retreive one page from the API.
    $these_results = uw_wcms_packages_gitlab_call($path . '?' . http_build_query($params));
    // Merge the results and exit the loop if there were none.
    if (count($these_results)) {
      $results = array_merge($results, $these_results);
    }
    else {
      break;
    }
  }

  return $results;
}
